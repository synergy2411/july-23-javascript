// import * as math from "./utils/maths";
// import getDailyQuote from "./utils/fortune";
// import data from "./loaders/data.json";
// import "./css/styles.css";

// console.log("JSON Data", data);

// console.log("Sum : ", math.sum(2, 4));
// console.log("Mul : ", math.mul(2, 4));
// console.log("Square : ", math.square(3));

// console.log("Daily Quote : ", getDailyQuote());

// const createIterator = (start = 0, end = 10, step = 1) => {
//   let counter = start;

//   let range = {
//     next() {
//       let result;
//       if (counter < end) {
//         result = { value: counter, done: false };
//         counter += step;
//         return result;
//       }
//       return { value: "completed", done: true };
//     },
//   };
//   return range;
// };

// const it = createIterator(1, 20, 2);

// for (let i = 0; i < 20; i++) {
//   console.log(it.next());
// }

// let arr = ["A", "B", "C"];

// console.log(arr);

// function* anotherGenerator() {
//   yield 101;
//   yield 102;
// }

// function* theGenerator() {
//   yield 1;
//   yield "Hello World";
//   yield true;
//   yield* arr;
//   yield* anotherGenerator();
// }

// const it = theGenerator();

// for (let item of theGenerator()) {
//   console.log(item);
// }

// async function* fetchData() {
//   try {
//     const responseOne = await fetch(
//       "https://jsonplaceholder.typicode.com/todos"
//     );
//     const todoCollection = responseOne.json();
//     yield todoCollection;

//     const responseTwo = await fetch(
//       "https://jsonplaceholder.typicode.com/posts"
//     );
//     const postCollection = responseTwo.json();
//     yield postCollection;
//   } catch (err) {
//     console.error(err);
//   }
// }

// async function consumePromise() {
//   const it = fetchData();
//   const resultOne = await it.next();
//   console.log("REsult One: ", resultOne.value);

//   const resultTwo = await it.next();
//   console.log("REsult Two: ", resultTwo.value);
// }

// consumePromise();

// MAP

// let obj = {};

// let user = {
//   name: "John Doe",
//   age: 32,
//   101: "number",
//   true: "Boolean",
//   obj: "Object",
// };

// // console.log(user);

// const map = new Map();

// map
//   .set("name", "Jenny")
//   .set(101, "number")
//   .set(true, "boolean")
//   .set(user, "Documents");

// for (let val of map.values()) {
//   console.log("VALUES : ", val);
// }

// for (let [key, value] of map.entries()) {
//   console.log(`${key} : ${value}`);
// }

// SET

// const set = new Set();

// set.add("John");
// set.add("Jenny");
// set.add("Jenny");
// set.add("Jack");

// console.log(set.size); // ?

// Weak Reference

// let obj = { name: "John" };

// let arr = [obj];

// obj = null;

// console.log(arr); // ?

// let jamesBond = { name: "007" };

// const wm = new WeakMap();

// wm.set(jamesBond, "Secret Documents");

// const ws = new WeakSet();
// ws.add(jamesBond);
// ws.add(jamesBond);
// ws.add(jamesBond);

// jamesBond = null;

// # New Features
// - Object.entries()

// let user = {
//   name: "John",
//   age: 32,
// };

// console.log(Object.entries(user));

// - Object.values()
// console.log(Object.values(user));

// - getOwnPropertyDescriptor()
// console.log(Object.getOwnPropertyDescriptor(user, "name"));

// - String padding (padStart(), padEnd())

// let str = "Hello World";

// console.log(str.padStart(20, "-"));

// let aadharNumber = "987654321012";

// const slicedValue = aadharNumber.slice(-4);

// const maskedAadharNumber = slicedValue.padStart(aadharNumber.length, "*");

// console.log(maskedAadharNumber);

// - Array - flat(), flatMap()
// let arr = [101, 102, [103, 104, [201, 202]], 105];

// let arr = [101, 102, 103, 104, 105];

// let mappedArray = arr.map((ele) => (ele > 104 ? ["New", "Element"] : ele));

// const flattenArray = mappedArray.flat(2);

// let flatMappedValue = arr.flatMap((el) => (el > 104 ? [110, 111] : el));

// console.log(flatMappedValue);

// - globalThis

// 'this' : self, frame, global

// const demoFn = () => {
//   console.log(globalThis);
// };

// demoFn();

// - ?. (optional chaining operator)

let person = {
  name: "Joe",
  address: {
    city: "Pune",
  },
};

console.log(person.state?.city);

// - ?? (nullish coalescing operator)
// - undefined & null

const demoFn = (name) => {
  const username = name ?? "Default Name";

  //   let username;

  //   if (name) {
  //     username = name;
  //   } else {
  //     username = "Default Name";
  //   }

  console.log(username); // ?
};

demoFn(0);
