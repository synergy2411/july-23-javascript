// PROMISES

// Builder / Producer

// const createPromise = (ms) => {
//   let promise = new Promise((resolve, reject) => {
//     if (ms > 3000) {
//       reject(new Error("Too high value"));
//     } else {
//       setTimeout(() => {
//         resolve({ data: "SUCCESS" });
//       }, ms);
//     }
//   });
//   return promise;
// };

// Consumer
// - then().catch()
// - Async...await : looks like sync code

// const consumePromise = async () => {
//   try {
//     const result1 = await createPromise(1000);
//     console.log("Result 1 :", result1);

//     const result2 = await createPromise(2000);
//     console.log("Result 2 :", result2);
//   } catch (err) {
//     console.error(err);
//   } finally {
//     console.log("FINALLY");
//   }
// };

// const consumePromise = () => {
//   createPromise(1000)
//     .then((result) => {
//       console.log("RESULT : ", result);
//       return result.data;
//     })
//     .then((resp) => {
//       console.log("REPSONSE : ", resp);
//     })
//     .catch((err) => console.error(err))
//     .finally(() => console.log("FINALLY"));
// };

// consumePromise();

//

// const buildPromise = (ms, state, data) => {
//   return new Promise((resolve, reject) => {
//     setTimeout(() => {
//       if (state === "fulfilled") {
//         resolve(data);
//       } else {
//         reject(new Error("Rejected"));
//       }
//     }, ms);
//   });
// };

// const promiseOne = buildPromise(1000, "rejected", "success 1");
// const promiseTwo = buildPromise(2000, "fulfilled", "success 2");
// const promiseThree = buildPromise(3000, "fulfilled", "success 3");

// const allPromises = [promiseTwo, promiseOne, promiseThree];

// Promise.any(allPromises)
//   .then((responses) => {
//     console.log("RESPONSES : ", responses);
//   })
//   .catch(console.error);

// Promise.all(allPromises)
//   .then((responses) => {
//     console.log("RESPONSES : ", responses);
//   })
//   .catch(console.error);

// Promise.allSettled(allPromises)
//   .then((responses) => {
//     responses.forEach((resp) => {
//       if (resp.status === "fulfilled") {
//         console.log("RESPONSE : ", resp);
//       }
//     });
//   })
//   .catch(console.error);

// Promise.resolve({ message: "SUCCESS" }).then(console.log);
// Promise.reject(new Error("Something went wrong"))
//   .then(console.log)
//   .catch(console.error);

let p1 = loadConfig({ recordId: this.recordId });
let p2 = setTimeout(() => {
  return loadConfig({ recordId: this.recordId });
}, 4000);
let p3 = setTimeout(() => {
  return loadConfig({ recordId: this.recordId });
}, 5000);

let allPromise = [p1, p2, p3];

Promise.any(allPromise).then((response) => {
  console.log("RESPONSE - ", response);
});

loadConfig({ recordId: this.recordId })
  .then((result) => {
    this.data = result;
  })
  .catch((error) => {
    window.setTimeout(() => {
      loadConfig({ recordId: this.recordId })
        .then((result) => {
          this.data = result;
        })
        .catch((error) => {
          window.setTimeout(() => {
            loadConfig({ recordId: this.recordId })
              .then((result) => {
                this.data = result;
              })
              .catch((error) => {
                console.log(error);
              });
          }, 4000);
        });
    }, 4000);
  });
