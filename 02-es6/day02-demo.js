// class Person {
//   #name;
//   constructor(name) {
//     this.#name = name;
//   }

//   getName() {
//     return this.#name;
//   }
// }

// class Employee extends Person {
//   static numberOfEmployees = 0;
//   constructor(name, designation) {
//     super(name);
//     this.designation = designation;
//     Employee.numberOfEmployees++;
//   }

//   getDetails() {
//     return `${super.getName()} - ${this.designation}`;
//   }
// }

// let jenny = new Employee("Jenny Alice", "Developer");
// console.log(jenny.getDetails());

// let jack = new Employee("Jack", "Admin");
// console.log(jack.getDetails());

// console.log("# of Employees -> ", Employee.numberOfEmployees);

// class Student extends Person {
//   static #enrolledStudent = 0;
//   constructor(studId, studName) {
//     super(studName);
//     this.studId = studId;
//     // this.studName = studName;
//     Student.#enrolledStudent++;
//   }

//   static getEnrolledStudents() {
//     return Student.#enrolledStudent;
//   }

//   getDetail() {
//     return `${this.studId} - ${super.getName()}`;
//   }
// }

// let john = new Student("S001", "John Doe");

// console.log(john.getDetail());

// console.log("Enrolled Students -> ", Student.getEnrolledStudents());

// class Animal {
//   #numberOfLegs;

//   constructor(species) {
//     this.species = species;
//   }

//   get legs() {
//     return this.#numberOfLegs;
//   }

//   set legs(value) {
//     this.#numberOfLegs = value;
//   }

//   #getSpecies() {
//     return this.species;
//   }
// }

// let bunny = new Animal("Rabbit");

// bunny.legs = 4; // setter method

// console.log("Bunny Legs : ", bunny.legs); // getter method

// console.log(bunny.getSpecies()); // ERROR - coz it's private

// class Animal {
//   #numberOfLegs;
//   #speed;

//   constructor(numberOfLegs) {
//     this.#numberOfLegs = numberOfLegs;
//   }
//   getLegs() {
//     return this.#numberOfLegs;
//   }

//   get speed() {
//     return this.#speed;
//   }
//   set speed(value) {
//     this.#speed = value;
//   }
// }

// let jerry = new Animal(4);

// jerry.speed = 10;

// console.log("Jerry Legs : ", jerry.getLegs());
// console.log("Jerry Speed : ", jerry.speed);

// ---------------
// ARROW FUNCTIONS

// function add(n1, n2) {
//   return n1 + n2;
// }

// const sum = (n1, n2) => n1 + n2;

// console.log("Sum : ", sum(2, 4));

// const mul = (n1, n2) => {
//   console.log("Numbers : ", n1, n2);
//   return n1 * n2;
// };

// console.log("Multiply : ", mul(2, 4));

// const square = (n1) => n1 * n1;

// console.log("Square : ", square(2));

// Constructor Function

// function Person(name, age){
//     this.name = name;
//     this.age = age;
// }

// const Person = (name, age) => {
//   console.log(this);
//   this.name = name;
//   this.age = age;
// };

// Person();
// let john = new Person("John", 32);

// function simpleDemoFn() {
//   console.log("Arguments : ", arguments);
// }

// simpleDemoFn("Mary Public", 23);

// const demoFn = () => {
//   console.log(arguments);       // ERROR - Not available in Arrow function
// };

// demoFn();

// let numbers = [1, 2, 3, 4, 5];

// const doubleValue = numbers.map(function (value) {
//   return value * 2;
// });

// const tripleValue = numbers.map((value) => value * 3);

// const filteredValue = numbers.filter((value) => value < 3);

// console.log(doubleValue);
// console.log(tripleValue);
// console.log(filteredValue);

// ----------
// REST :
// OUTPUT -> Create the collection(Array);
// INPUT -> individual elements

// const demoFn = (email, age,...args) => {
//   console.log(args[0]); // ?
// };

// // demoFn("john@test")
// // demoFn("john@test", 32)
// demoFn("john@test", 32, true);

// SPREAD
// OUTPUT -> individual items
// INPUT -> Collection (Array / Object)

// const marks = [96, 95, 93];

// const demoFn = (mark1, mark2, mark3) => {
//   console.log(mark1, mark2, mark3);
// };

// demoFn(...marks);

// let numbers = [3, 4, 5];
// let moreNumbers = [1, 2, ...numbers, 6, 7, 8];

// console.log(moreNumbers);

// let userOne = {
//   name: "John",
//   email: "john@test.com",
//   company: "Apple",
// };

// let userTwo = {
//   ...userOne,
//   name: "Jenny",
//   email: "jenny@test.com",
// };

// let userThree = {
//   name: "Alice",
//   email: "alice@test.com",
// };

// console.log(userTwo);

// const assignedUser = Object.assign({}, userOne, userThree, { isWorking: true });

// console.log("ASSIGNED : ", assignedUser);

// Shallow Copy - Some part is still connected
// Reference will be connected
// Primitive will be copied

// let userOne = {
//   name: "John",
//   friends: ["Joe", "Monica"],
// };

// let userTwo = {
//   ...userOne,
// };

// userOne.name = "Jenny";

// userOne.friends.push("Rachel");

// userTwo.friends.push("Geller");

// console.log("User Two", userTwo);
// console.log("User One", userOne);

let userOne = {
  name: "Alice",
  friends: ["Joe", "Monica"],
};

// Deep Copy

const deepCopy = JSON.parse(JSON.stringify(userOne));

userOne.friends.push("Chandler");

console.log(deepCopy);
