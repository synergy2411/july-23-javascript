window.onload = function () {
  const listContainer = document.querySelector("#list-container");
  const btnAddItem = document.querySelector("#btnAddItem");
  const txtInput = document.querySelector("#txtInput");

  btnAddItem.addEventListener("click", function (event) {
    event.preventDefault();
    let newTodo = {
      label: txtInput.value,
      status: "pending",
    };

    fetch("http://localhost:3000/todos", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(newTodo),
    })
      .then((response) => response.json())
      .then((result) => {
        console.log("RESULT : ", result);
        listContainer.innerHTML = "";
        fetchTodos();
      })
      .catch(console.error);
  });

  async function fetchTodos() {
    try {
      const response = await fetch("http://localhost:3000/todos");
      const todoCollection = await response.json();
      todoCollection.forEach((todo) => {
        const liElement = document.createElement("li");
        liElement.innerHTML = `${todo.label.toUpperCase()}`;
        listContainer.appendChild(liElement);
      });
    } catch (err) {
      console.error(err);
    }
  }

  fetchTodos();
};
