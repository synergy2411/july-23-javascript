// Destructuring

// let fruits = ["Apple", "Banana", "Grapes", "Oranges"];

// let [, , , f4, f5] = fruits;

// console.log(f5); // ?

// const [ state, setState ] = useState()

// let userOne = {
//   firstName: "John",
//   lastName: "Doe",
//   age: 32,
//   email: "john@test.com",
// };

// let userTwo = {
//   email: "jenny@test.com",
//   age: 34,
//   firstName: "Jenny",
//   lastName: "Public",
// };

// let { email: e1, lastName: ln1 } = userOne;
// let { email: e2, lastName: ln2 } = userTwo;

// console.log(e1, ln2);

// let user = {
//   name: "John Doe",
//   address: {
//     city: "Pune",
//     state: "Maharastra",
//   },
//   friends: ["Joe", "Jack", "Jill"],
// };

// let {
//   name,
//   address: { city, state },
//   friends: [f1, f2, f3],
// } = user;

// console.log(name, state, f2);

// let users = [
//   {
//     name: "John",
//     age: 32,
//   },
//   {
//     name: "Jill",
//     age: 34,
//   },
//   {
//     name: "Jame",
//     age: 35,
//   },
// ];

// let [u1, u2, u3] = users;

// u1.name = "Alice";

// console.log(users); // ?

// users[1].name = "Mary";

// console.log(u2); // ?

// let [
//   { name: n1, age: a1 },
//   { name: n2, age: a2 },
//   { name: n3, age: a3 }] =
//   users;

// console.log(n1, a1);
