// Encapsulation
const LUCKY_NUMBER = Math.round(Math.random() * 100);

const getMyLuckyNumber = () => LUCKY_NUMBER;

const getDailyQuote = () => "Run 5 miles today!";

const getMealForToday = () => "Drink 2 ltr water";

// Named Export
export { getMyLuckyNumber, getDailyQuote };

// module.exports = {
//   getMyLuckyNumber,
//   getDailyQuote,
// };

// Default Export
export default getMealForToday;
// module.exports = getMealForToday;
