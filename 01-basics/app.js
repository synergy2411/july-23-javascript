// 'this' keyword

// - method

// let box = {
//   books: ["book1", "book2", "book3"],
//   addBook: function (book) {
//     this.books.push(book);
//   },
// };

// let anotherBox = {
//   books: ["book101", "bo0k102"],
// };

// box.addBook("book4");

// // Changing the context at run time
// box.addBook.call(anotherBox, "book103");

// console.log(box);
// console.log(anotherBox);

// SIMPLE FUNCTION CALL
// - refers to the global variable of the environment

// function demoFn() {
//   console.log("this inside function ->", this);
// }

// demoFn();

// function Person(name, age) {
//   // let this = {};

//   this.name = name;
//   this.age = age;
//   this.sayHello = function () {
//     console.log("Hello World!");
//   };
//   // return this;
// }

// let john = new Person("John", 23);
// console.log(john);
// john.sayHello();

// let jenny = Person("Jenny", 32);
// console.log(jenny);     // undefined
// jenny.sayHello();

// ARROW FUNCTION
// - 'this' resolved from the surrounded context

// let user = {
//   firstName: "John",
//   lastName: "Doe",
//   getDetails: function () {
//     // return this.firstName + " " + this.lastName;
//     let that = this;

//     function nestedFn() {
//       // console.log(this); // Global Object
//       return that.firstName + " " + that.lastName;
//     }

//     return nestedFn;
//   },
// };

// let user = {
//   firstName: "John",
//   lastName: "Doe",
//   getDetails: function () {
//     const nestedFn = () => this.firstName + " => " + this.lastName;
//     return nestedFn;
//   },
// };

// const xyz = user.getDetails();

// console.log(xyz());

// console.log(user.getDetails());

// console.log("Outside arrow ->", this);

// const demo = () => console.log(this);

// demo();

// function demoFn() {
//   console.log("DEMO FN : ", this);    // undefined
// }

// demoFn();

// alert("Hello World!");

const path = require("path");

let url = "http://www.example.com/public/index.html";

console.log(path.basename(url));
