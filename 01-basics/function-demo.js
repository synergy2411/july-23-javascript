// Function Expression
// var addition = function (n1, n2) {
//     return n1 + n2;
// };

// console.log(addition(4, 6));
// console.log(add(2, 4));

// function mystry() {
//   var chooseNumber = function () {
//     return 7;
//   };
//   return chooseNumber;
//   var chooseNumber = function () {
//     return 12;
//   };
// }

// const nestedFn = mystry();

// console.log(nestedFn());

// setTimeout(function () {
//     console.log("1 Second passed.")
//  }, 1000)

// Higher Order Function -> bounds the callback
// function demoGreet(arr, callback) {
//   if (arr.length > 2) {
//     callback(null, "Too high values");
//   } else {
//     callback(new Error("Too low Value"));
//   }
// }

// demoGreet([1], function (err, msg) {
//   if (err) {
//     console.error(err);
//     return;
//   }
//   console.log(msg);
// });

// Case 2 - Binding function with other objects

// function greet(msg) {
//   //   console.log("Hello ", this.name);
//   console.log(`${msg} - by ${this.name}`);
// }

// let userOne = {
//   name: "Jack",
//   //   greet: function () {
//   //     console.log("Hello " + this.name);
//   //   },
// };
// let userTwo = {
//   name: "Jenny",
//   //   greet: function () {
//   //     console.log("Hello " + this.name);
//   //   },
// };

// // userOne.greet();
// // userTwo.greet();

// // const boundedFn1 = greet.bind(userOne);
// // const boundedFn2 = greet.bind(userTwo);

// // boundedFn1();
// // boundedFn2();

// greet.call(userOne, "Guter Morgan");

// greet.apply(userTwo, ["Hola"]);

// CLOSURE

// function demo() {
//   let x = 4;
//   return function () {
//     return ++x;
//   };
// }

// const nestedFn = demo();

// console.log(nestedFn()); // 5
// console.log(nestedFn()); // 6
// console.log(nestedFn()); // 7

// function buildTicket(transport) {
//   let numOfPassengers = 0;

//   return function (name) {
//     return `
//         Hello ${name},
//         You are going via ${transport}!
//         Your ticket ID # : ${++numOfPassengers}.
//         `;
//   };
// }

// const shipFn = buildTicket("Ship");

// console.log(shipFn("John")); // ?
// console.log(shipFn("Jenny")); // ?

// const carFn = buildTicket("Car");

// console.log(carFn("Alice")); // ?

// Currying
// function demo(f) {
//   return function (a) {
//     return function (b) {
//       return f(a, b);
//     };
//   };
// }

// const sum = function (n1, n2) {
//   return n1 + n2;
// };

// const nestedFn = demo(sum);

// console.log(nestedFn(2)(3));            // Currying

// const plusThreeFn = nestedFn(3);        // Partial Function

// console.log(plusThreeFn(5));
// console.log(plusThreeFn(3));
// console.log(plusThreeFn(4));

// const plusFiveFn = nestedFn(5);

// console.log(plusFiveFn(10));

// Scoping

let x = 201;

// Case 1
// function a() {
//   let x = 301;
//   function b() {
//     console.log(x); // ?
//   }
//   b();
// }

// Case 2

function b() {
  console.log(x); // ?
}

function a() {
  var x = 301;
  b();
}

a();
