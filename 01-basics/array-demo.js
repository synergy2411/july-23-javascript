// Constructor Method

// function Person(firstName, lastName) {
//   this.firstName = firstName;
//   this.lastName = lastName;
//   //   this.getFullName = function () {
//   //     return this.firstName + " " + this.lastName;
//   //   };
// }

// Person.prototype.getFullName = function () {
//   return this.firstName + " " + this.lastName;
// };

// let john = new Person("John", "Doe");

// let jenny = new Person("Jenny", "Alice");

// console.log(jenny.getFullName());

// console.log(john.getFullName());

// let str = new String("Hello World");

// console.log(str.length);

// let arr = [
//   "John",
//   32,
//   true,
//   function () {
//     console.log("Hello there!");
//   },
//   {},
//   ["Foo", "Bar"],
// ];

// arr[3]();

// let friends = ["Monica", "Joe", "Chandler", "Ross"];

// friends.push("Rachel");

// friends.unshift("Geller");

// friends.pop();

// friends.shift();

// friends.sort();

// friends.reverse();

// friends.fill("Ross", 0, 2);

// Transformed Value
// const newFriends = friends.map(function (value) {
//   return "My Friend - " + value;
// });

// Filter based upon some condition
// let filteredArray = friends.filter(function (value) {
//   return value.includes("a");
// });

// let resultArray = [];

// for (let i = 0; i < friends.length; i++) {
//   resultArray.push("My New Friend - " + friends[i]);
// }

// const slicedArray = friends.slice(0, 2);

// const concatedArray = friends.concat("Geller");

// const position = friends.indexOf("Joe");

// Return position based upon some condition
// const position = friends.findIndex(function (value) {
//   return value === "Ross";
// });

// const deletedElements = friends.splice(0, 2);

// Returns true if condition met at least one time
// const isContainA = friends.some(function (value) {
//   return value.includes("a");
// });

// Returns true if condition meet for all elements
// const isContainA = friends.every(function (value) {
//   return value.includes("a");
// });

// const joinedArray = friends.join(", ");

// const newArray = friends.forEach(function (value) {
//   if (value.includes("J")) {
//     console.log(value);
//   }
// });

// const reducedArray = friends.reduce(function (prev, curr) {
//   console.log(prev, curr);
//   return prev + " - " + curr;
// });

// console.log(friends);
// console.log(reducedArray);

// let numbers = [2, 4, 6, 8, 10];

// const reducedNumber = numbers.reduce(function (prev, curr) {
//   return prev + curr;
// });

// console.log(reducedNumber);

// console.log(newArray);

// console.log(joinedArray);

// console.log(deletedElements);
// console.log(position);

// console.log(newFriends);
// console.log(filteredArray);

// console.log(friends.includes("Rachel"));

// -------

let users = [
  { name: "John", email: "john@test", age: 32, isAdmin: true },
  { name: "Jack", email: "jack@test", age: 34, isAdmin: false },
  { name: "Jill", email: "jill@test", age: 36, isAdmin: true },
];

// Print all the admin users name
// const allAdmins = users.filter(function (user) {
//   return user.isAdmin;
// });

const adminUserNames = users.map(function (user) {
  if (user.isAdmin) return user.name;
  //   return user.isAdmin && user.name;
});

console.log(adminUserNames.join(" "));

// Print the average age of users
const totalAge = users.reduce(function (prev, curr) {
  return prev + curr.age;
}, 0);

console.log("Average Age : ", totalAge / users.length);

// Change Jill user name to Jenny

users.forEach(function (user) {
  if (user.name === "Jill") user.name = "Jenny";
});

console.log("Changed Name : ", users[2]);

// Delete all the users having age < 35
const filteredUsers = users.filter(function (user) {
  return user.age > 35;
});

console.log("Filtered Users : ", filteredUsers);
