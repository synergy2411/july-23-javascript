# Break Timing -

- Tea Break : 11:00AM - 11:15AM

# What is JavaScript?

- Client side (Browser) as well as Server Side (Node Runtime Environment)
- Scripting language
- Interactive / Dynamic
- Single Page App
- Many Libs and Frameworks (React, Angular, jQuery, Vue, Next etc)
- Compoents (like Callouts)
- Multiple platforms (Web Browsers, Native Devices, Server side)
- Object Oriented (ES6 / ES2015)
- Extend the functionalities of other libs
- Single Threaded
- Asynchronous Programming / Non Blocking
- JavaScript Engine
- Vast community support
- Light-weight
- Compiled and interpreted
- Open Source
-

- MEAN -> MongoDB Express Angular Node
- MERN -> MongoDB Express React Node

- MongoDB -> Stores data in the form of JSON
- Express -> Web Framework (JavaScript)
- Angular / React -> JS framework / JS Library
- Node -> Platform to execute JS

# JS Engine executes

- All the Synchronous operation
- All the Asynchronous operation
  - MicroTask : Promise, queueMicrotask()
  - MacroTask : timers, XHR Call, any other async operation

# Block Scoping Variables

- restricts the scope of the variable to the nearest block
- if...else, while, for etc

- var keyword scope - function scope

# JavaScript Engine exectues in two phase

- Creational Phase : memory is allocated; JS Engine assigns 'undefined' to all the variables
- Executional Phase : code is executed line by line;

# undefined > Special value supplied by JS Engine

# not defined > ERROR

# Operators -

- Arithematic : \* / + -
- Logical : && || !
- Comparison : < > <== >== !==

# DOM -> Hierarchical representation of actual HTML Elements

# Object Hierarchy

magicShoe -> shoe -> Object

# Data Types in JS-

- Primitive Types
- Object/Reference Types

# Object Creational Methods

- Literal { }
- Instance Object.create() : implementing inheritance
- Constructor : similar type of objects

# Array

# Functions

# Scope

# this keyword

# Object Hierarchy / Prototype Chain

Object (isPrototypeOf, hasOwnProperty, constructor, valueOf, toString etc)

String (toUpperCase, toLowerCase, charAt, indexOf, includes, *length) -> str
Number (toFixed, isNan())
Boolean (isBoolean)
Function (call, apply, bind, *name)
Array (push, pop, every, map, forEach, *length etc)
Date (getMonth, setMonth etc)
Person (*firstName, \*lastName, getFullName()) -> john

str.toString()

# Function Declaration

- take place into memory in Creational Phase (Hoisted)
- JS Engine does NOT re-evalute such functions
- Good for code which you don't want to re-evaluate

# Function Expression

- take place into memory in Executional Phase (Never Hoisted)
- JS Engine does re-evalute such functions at runtime

# Error First, Callback Last

# Closure - Process of binding the outer scope of variable with the nested function

# Currying

- f(a,b,c) -> f(a)(b)(c)

- Partially calling the function -> f(a)

# Lexical Scope (Outer scope of function) is determined by the placement of function

## globalEnv -> funcAEnv -> funcBEnv

# this keyword

# DOM Object

# Internal of JS Engine

# Modern JavaScript

# Shadow DOM

# -------------

# Class - introduced in ES6

- extends keyword
- static members
- private members (#)
- everything is public
- Accessor / mutator

# ------------

- Arrow Function
- Rest / Spread
- Class Syntax
- this keyword

# ------------

- Modern JS : Destructuring, Promise API, fetch API
- Module (ESM)
- Webpack & Babel
- Best practices for JS

## Promise API Static Methods

- all : all-or-nothing concept
- allSettled : resolved promises values
- race : first completed promise will "win the race" (p1)
- any : first successfuly completed promise will "win the race" -(p2)
- resolve : immediately resolved promise
- reject : immediately reject promise

p1 = 100 (reject)
p2 = 200 (success)
p3 = 300 (success)

> npm i json-server -g
> created JSON file
> json-server --watch db.json

# ---------------

# Module -

- ECMA Script Modules (ESM) - browsers
  > export
  > import statement
- CommonJS Module - NodeJS
  > module.exports
  > require()
- AMD - Async Module Definition
- UMD - Universal Module Definition

# Node Installer

- Node Runtime Environment (NRE)
- Node Package Manager (NPM)
- Node Native Module (40+) - eg. path, os, http, events, utils etc

# ---------------

# to generate package.json file

> npm init [-y]
> npm i webpack webpack-cli -D

# Webpack - Module Bundler

- Module loading
- uglfying and minifying
- optimization
- asset management
- injecting the env variable
- other types of loaders - jpg, raw file, css file etc

# babel packages

> npm i @babel/core @babel/cli @babel/preset-env babel-loader -D

# Iterator & Generator

- Iterators are capaable to iterate using for...of loop
- Array internally implements Iterable protocol
- next() => {value : type , done : boolean}

- Generators are the special function who does not have continuous execution
- Generator returns the iterator object
- 'yield' keyword

# Generator Use case

- Redux-saga
- Lazy evaluation

# Data Structure - Object & Array

- Map : stores key-value data
- Set : stores unique data

- new Map() | new Set()
- set()
- get()
- has()
- delete()
- clear()
- size

# Weak references - Garbage collector will removes the object along with all of its references

- WeakMap : Receives the object as key
- WeakSet : Receives the object as value

# New Features

- Object.entries() : returns array created by the object properties
- Object.values() : return values of object properties as array elements
- getOwnPropertyDescriptor() : returns object describing the mentioned property
- String padding (padStart(), padEnd()) : add padding to start / end
- Array - flat(), flatMap() : flatten the array to the mentioned depth
- globalThis : always refers to 'global this' keyword
- ?. (optional chaining operator) : returns undeined, if property does not exist
- ?? (nullish coalescing operator) : return RHS value if LHS value is either undefined / null.
- if -> 0, false, undefined, null
